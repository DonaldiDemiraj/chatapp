const form = document.querySelector(".typing-area"),
inputField = form.querySelector("input-field"),
sendBtn = form.querySelector("button"),
chatBox = document.querySelector(".chat-box");

form.onsubmit = (e)=>{
    e.preventDefault();
}

sendBtn.onclick = ()=>{
    //ajax
    let xhr = new XMLHttpRequest(); //creating xml object
    xhr.open("POST", "php/insert-chat.php", true);
    xhr.onload = ()=>{
        // debugger;
        if(xhr.readyState === XMLHttpRequest.DONE){
            if(xhr.status === 200){
                inputField.value = ""; //once message inserted into database
                scrollToBottom();
            }
        }
    }

    // we have to send the form data throught ajax to php

    let formData = new FormData(form); //creating new formData Object
    xhr.send(formData); //sending the form data to php
}

chatBox.onmouseenter = ()=>{
    chatBox.classList.add("active");
}

chatBox.onmouseleave = ()=>{
    chatBox.classList.remove("active");
}

setInterval(()=>{
    //ajax
    let xhr = new XMLHttpRequest(); //creating xml object
    xhr.open("POST", "php/get-chat.php", true);
    xhr.onload = ()=>{
        // debugger;
        if(xhr.readyState === XMLHttpRequest.DONE){
            if(xhr.status === 200){
                let data = xhr.response;
                chatBox.innerHTML = data;
                if(!chatBox.classList.contains("active")){
                    scrollToBottom();
                }
            }
        }
    }
    let formData = new FormData(form); //creating new formData Object
    xhr.send(formData); //sending the form data to php
}, 500);


function scrollToBottom(){
    chatBox.scrollTop = chatBox.scrollHeight;
}