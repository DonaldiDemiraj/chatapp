const form = document.querySelector(".login form"),
contineuBtn = form.querySelector(".button input"),
errorText = form.querySelector(".error-txt");

form.onsubmit = (e)=>{
    e.preventDefault()//for submit
}

contineuBtn.onclick = ()=>{
    //ajax
    let xhr = new XMLHttpRequest(); //creating xml object
    xhr.open("POST", "php/login.php", true);
    xhr.onload = ()=>{
        // debugger;
        if(xhr.readyState === XMLHttpRequest.DONE){
            if(xhr.status === 200){
                let data = xhr.response;
                if(data == "success!"){
                    location.href = "users.php";
                }else{
                    errorText.textContent = data;
                    errorText.style.display = "block";
                }
            }
        }
    }

    // we have to send the form data throught ajax to php

    let formData = new FormData(form); //creating new formData Object
    xhr.send(formData); //sending the form data to php
}