<?php
    session_start();
    include_once "config.php";
    $fname = mysqli_real_escape_string($conn, $_POST['fname']);
    $lname = mysqli_real_escape_string($conn, $_POST['lname']);
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $password = mysqli_real_escape_string($conn, $_POST['password']);
    // $image = mysqli_real_escape_string($conn, $_POST['']);

    if(!empty($fname) && !empty($lname) && !empty($email) && !empty($password)){
        //check if user wmail is valid
        if(filter_var($email, FILTER_VALIDATE_EMAIL)){ //if email is valid
            //lets check if email already exist in database or not 
            $sql =  mysqli_query($conn, "SELECT email FROM users WHERE email = '{$email}'");
            if(mysqli_num_rows($sql) > 0){// if email already exist
                echo "$email - This email already exist";
            }else{
                //check if user upload files or not 
                if(isset($_FILES['image'])){ //if file is uploaded 
                    $img_name = $_FILES['image']['name']; //geting user uploaded img name
                    $tmp_name = $_FILES['image']['tmp_name']; //this temp name is used to save files in our folder

                    //explore img and get the last name extension like jpg png
                    $img_explode = explode('.', $img_name);
                    $img_ext = end($img_explode); //here we get the extension of an user uploaded img file

                    $extension = ['png', 'jpeg', 'jpg']; //same valid img 
                    if(in_array( $img_ext,  $extension) === true){ //
                        $time = time(); //this will return us curent time 

                        //move user upload image to our foler
                        $new_img_name = $time.$img_name;

                        if(move_uploaded_file($tmp_name, "images/".$new_img_name)){ //if user upload img move to our folder successfully
                            $status = "Active now"; //once user signet up then his status will be active
                            $random_id = rand(time(), 10000000); //create random id foer user

                            //insert all user data inside a table 
                            $sql2 = mysqli_query($conn, "INSERT INTO users (unique_id, fname, lname, email, password, img, status)
                                                        VALUES ({$random_id}, '{$fname}', '{$lname}', '{$email}' ,'{$password}', '{$new_img_name}', '{$status}')");
                            if($sql2){// if there data inserted
                                $sql3 = mysqli_query($conn, "SELECT * FROM users WHERE email = '{$email}'");
                                if(mysqli_num_rows($sql3) > 0){
                                    $row = mysqli_fetch_assoc($sql3);
                                    $_SESSION['unique_id'] = $row['unique_id']; //session for used user unique_id in other php file
                                    echo "success!";
                                }
                            }else{
                                echo "Something Wrong!";
                            }
                        }
                    }else{
                        echo "Please select an Image files - jpeg, jpg, png !";
                    }
                }else{
                    echo "Please select a image!";
                }
            }
        }else{
            echo "$email - This is not a valid email!";
        }
    }else{
        echo "All input filed are required!";
    }
?>