<?php 
    session_start();
    if(!isset($_SESSION['unique_id'])){
        header("location: login.php");
    }
?>

<?php include_once "header.php"; ?>
<body>
    <div class="wrapper">
        <section class="form login"> 
            <header>Realtime Chat App</header>
            <form action="#">
                <div class="error-txt">
                </div>
                <div>
                    <div class="field input">
                        <label>Email Address</label>
                        <input type="text" name="email" placeholder="Enter Your Email">
                    </div>
                    <div class="field input">
                        <label>Password</label>
                        <input type="password" name="password" placeholder="Enter Your Password">
                        <i class="fa fa-eye"></i>
                    </div>
                    <div class="field button">
                        <input type="submit" value="Continue to Chat">
                    </div>
                </div>
            </form>
            <div class="link">
                Not yet singed up? <a href="index.php">SignUp Now   </a>
            </div>
        </section>
    </div>

    <script src="./js/pass-show-hiden.js"></script>
    <script src="./js/login.js"></script>

</body>
</html>